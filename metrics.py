def accuracy(y_true, y_pred):
    size = len(y_true)
    if size != len(y_pred):
        print("Accuracy: labels size error")
        return 0
    correct = 0
    for i in range(0, size):
        if y_true[i] == y_pred[i]:
            correct += 1
    return correct / size
