from evi_rules_classifier.AssociativeClassifier import AssociativeClassifier


class EviRC(AssociativeClassifier):

    def __init__(self, rules, class_attribute, class_domain):
        valid_rules = []
        for r in rules:
            if r["lift"] > 1 and r["per"] < 0.5:
                valid_rules.append(r)
        super().__init__(valid_rules, class_attribute)
        self.class_domain = class_domain
        self.class_attribute = class_attribute
        self.filtered_rules = EviRC.filter_rules(self.classification_rules)
        self.frequent_label = self.frequent_class_label()

    @staticmethod
    def filter_rules(rules):
        rules = sorted(rules, key=lambda rule: (rule["confidence"], rule["support"]), reverse=True)
        i = 0
        while i < len(rules):
            j = i + 1
            while j < len(rules):
                rule1 = rules[i]
                rule2 = rules[j]
                go_next_step = True
                attributes1 = rule1["antecedent"].split(";")
                attributes2 = rule2["antecedent"].split(";")
                # attribute2 includes attribute1
                is_subset = all(attribute in attributes2 for attribute in attributes1)
                if is_subset and rule1["confidence"] > rule2["confidence"]:
                    go_next_step = False
                    del rules[j]
                if go_next_step:
                    j += 1
            i += 1
        return rules

    def classify(self, instance):
        instance_attributes = {}
        instance_data = {}
        instance_masses = {}
        instance_focal_elements = []
        for attr in instance:
            instance_attributes[attr] = []
            instance_data[attr] = []
            instance_masses[attr] = []
            for focal_element in instance[attr]:
                instance_attributes[attr].append(attr+"#"+focal_element)
                instance_data[attr].append({"focal_element": focal_element, "mass": instance[attr][focal_element]})
                instance_masses[attr].append(instance[attr][focal_element])
                instance_focal_elements.append(attr + "#" + focal_element)
        rules = self.filtered_rules
        i = 0
        while i < len(rules):
            rule_parts = rules[i]["antecedent"].split(";")
            rule_mass = 1
            total_intersections = 0
            for rule_part in rule_parts:
                attribute_mass = 0
                elements = rule_part.split("#")
                rule_focal_elements = set(elements[1].split(","))
                instance_values = []
                if elements[0] in instance_data:
                    instance_values = instance_data[elements[0]]
                for fe in instance_values:
                    values = set(fe["focal_element"].split(","))
                    intersection = len(values & rule_focal_elements)
                    union = len(values | rule_focal_elements)
                    if intersection > 0:
                        attribute_mass += fe["mass"] * intersection / union
                if attribute_mass > 0:
                    total_intersections += 1
                rule_mass *= attribute_mass
            if total_intersections == len(rule_parts):
                rules[i]["reliability"] = rule_mass
                i += 1
            else:
                del rules[i]
        if len(rules) == 0:
            self.unclassified_instances += 1
            return self.frequent_label.replace(self.class_attribute + "#", "")
        bbas = []
        for rule in rules:
            contents = rule["consequent"].split("#")
            rule_class = contents[1]
            domain = self.class_domain.copy()
            domain.remove(rule_class)
            other = ",".join(domain)
            rule_reliability = rule["confidence"] * rule["reliability"]
            other_reliability = (1 - rule["confidence"]) * rule["reliability"]
            bbas.append({
                rule_class: round(rule_reliability, 2),
                other: round(other_reliability, 2),
                ",".join(self.class_domain): round(1 - rule_reliability - other_reliability, 2)
            })
        final_bba = bbas[0]
        lenght = len(bbas)
        for i in range(1, lenght):
            final_bba = EviRC.dempster_combination(final_bba, bbas[i])
        values = {}
        for element in self.class_domain:
            values[element] = round(EviRC.pignistic(set(element), final_bba), 3)
        values = sorted(values.items(), key=lambda item: item[1], reverse=True)
        return values[0][0]
