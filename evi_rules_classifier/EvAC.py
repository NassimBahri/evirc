from evi_rules_classifier.AssociativeClassifier import AssociativeClassifier
from enum import Enum
import numpy as np
import math


class RuleFilter(Enum):
    PRECISE = "precise"
    GENERIC = "generic"


class EvAC(AssociativeClassifier):

    def __init__(self, rules, class_attribute, class_domain, rules_filter):
        super().__init__(rules, class_attribute)
        self.class_domain = class_domain
        self.class_attribute = class_attribute
        if rules_filter != RuleFilter.GENERIC and rules_filter != RuleFilter.PRECISE:
            raise Exception("Unknown value for the filter variable")
        if rules_filter == RuleFilter.GENERIC:
            self.filtered_rules = EvAC.filter_generic_rules(self.classification_rules)
        else:
            self.filtered_rules = EvAC.filter_precise_rules(self.classification_rules)
        self.frequent_label = self.frequent_class_label()

    @staticmethod
    def filter_generic_rules(rules):
        rules = sorted(rules, key=lambda rule: len(rule["antecedent"].split(";")), reverse=False)
        i = 0
        while i < len(rules):
            j = i + 1
            while j < len(rules):
                rule1 = rules[i]
                rule2 = rules[j]
                go_next_step = True
                if rule1["consequent"] == rule2["consequent"]:
                    attributes1 = rule1["antecedent"].split(";")
                    attributes2 = rule2["antecedent"].split(";")
                    # attribute2 includes attribute1
                    is_subset = all(attribute in attributes2 for attribute in attributes1)
                    if is_subset:
                        go_next_step = False
                        del rules[j]
                if go_next_step:
                    j += 1
            i += 1
        return rules

    @staticmethod
    def filter_precise_rules(rules):
        rules = sorted(rules, key=lambda rule: len(rule["antecedent"].split(";")), reverse=True)
        i = 0
        while i < len(rules):
            j = i + 1
            while j < len(rules):
                rule1 = rules[i]
                rule2 = rules[j]
                go_next_step = True
                if rule1["consequent"] == rule2["consequent"]:
                    attributes1 = rule1["antecedent"].split(";")
                    attributes2 = rule2["antecedent"].split(";")
                    # attribute2 includes attribute1
                    is_subset = all(attribute in attributes1 for attribute in attributes2)
                    if is_subset:
                        go_next_step = False
                        del rules[j]
                if go_next_step:
                    j += 1
            i += 1
        return rules

    def classify(self, instance):
        instance_attributes = {}
        instance_masses = {}
        instance_focal_elements = []
        for attr in instance:
            instance_attributes[attr] = []
            instance_masses[attr] = []
            for focal_element in instance[attr]:
                instance_focal_elements.append(attr+"#"+focal_element)
                instance_attributes[attr].append(attr+"#"+focal_element)
                instance_masses[attr].append(instance[attr][focal_element])
        rules = self.filtered_rules
        i = 0
        while i < len(rules):
            parts = rules[i]["antecedent"].split(";")
            intersection = any(fe in parts for fe in instance_focal_elements)
            if intersection:
                i += 1
            else:
                del rules[i]
        if len(rules) == 0:
            self.unclassified_instances += 1
            return self.frequent_label.replace(self.class_attribute + "#", "")
        bbas = []
        vacuous = ",".join(self.class_domain)
        for rule in rules:
            rule["reliability"] = EvAC.compute_reliability(rule, {"focal_element": instance_attributes,
                                                                  "masses": instance_masses})
            rule_reliability = rule["confidence"] * rule["reliability"]
            bbas.append({
                rule["consequent"].replace(self.class_attribute+"#", ""): round(rule_reliability, 2),
                vacuous: round(1 - rule_reliability, 2)
            })
        final_bba = bbas[0]
        lenght = len(bbas)
        for i in range(1, lenght):
            final_bba = EvAC.dempster_combination(final_bba, bbas[i])
        values = {}
        for element in self.class_domain:
            values[element] = round(EvAC.pignistic(set(element), final_bba), 3)
        values = sorted(values.items(), key=lambda item: item[1], reverse=True)
        return values[0][0]

    @staticmethod
    def compute_reliability(rule, instance):
        premise_attributes = rule["antecedent"].split(";")
        distance = 0
        for attribute, focal_elements in instance["focal_element"].items():
            premise_attribute = ""
            for attr in premise_attributes:
                if attr.startswith(attribute+"#"):
                    premise_attribute = attr
            distance += EvAC.jousselme_distance(premise_attribute, focal_elements, instance["masses"][attribute])
        return 1 - distance / len(premise_attributes)

    @staticmethod
    def jousselme_distance(rule_attribute, focal_elements, masses):
        if rule_attribute == "":
            return 0
        elements = set(())
        if rule_attribute != "":
            elements.add(rule_attribute)
        for focal_element in focal_elements:
            elements.add(focal_element)
        m1 = np.zeros((len(elements), 1))
        m2 = np.zeros((len(elements), 1))
        i = 0
        for element in elements:
            if element == rule_attribute:
                m1[i] = 1
            if element in focal_elements:
                m2[i]= masses[focal_elements.index(element)]
            i += 1
        m3 = m1 - m2
        m3_t = np.transpose(m3)
        D = np.zeros((len(elements), len(elements)))
        items = []
        for element in elements:
            parts = element.split("#")
            items.append(parts[1].split(","))
        i = 0
        for item1 in items:
            j = 0
            for item2 in items:
                D[i][j] = len(np.intersect1d(item1, item2)) / len(np.union1d(item1, item2))
                j += 1
            i += 1
        distance = m3_t.dot(D)
        distance = distance.dot(m3)
        distance = round(math.sqrt(0.5 * distance), 2)
        return distance

