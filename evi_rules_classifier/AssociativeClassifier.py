from abc import ABCMeta, abstractmethod


class AssociativeClassifier(metaclass=ABCMeta):

    def __init__(self, rules, class_attribute):
        self.classification_rules = []
        self.filtered_rules = []    # rules after pruning
        for rule in rules:
            parts = rule["consequent"].split("#")
            if parts[0] == class_attribute:
                self.classification_rules.append(rule)
        self.unclassified_instances = 0

    def predict(self, instances):
        if len(self.filtered_rules) == 0:
            print("No rules found")
            return []
        classes = []
        i = 0
        for instance in instances:
            classes.append(self.classify(instance))
            i += 1
        return classes

    @abstractmethod
    def classify(self, instance):
        pass

    def frequent_class_label(self):
        labels = {}
        for r in self.filtered_rules:
            if r["consequent"] not in labels:
                labels[r["consequent"]] = 0
            labels[r["consequent"]] += 1
        labels = sorted(labels.items(), key=lambda item: item[1], reverse=True)
        if len(labels) == 0:
            return None
        return labels[0][0]

    @staticmethod
    def dempster_combination(bba1, bba2):
        # bba format: {"1": 0.031, "2": 0.186, "1,3": 0.093, "1,2,3": 0.69}
        bba = {}
        # Extract focal elements
        focal_elements = set(bba1.keys())
        focal_elements = focal_elements.union(set(bba2.keys()))
        for focal_element in focal_elements:
            quotient = 0
            divisor = 0
            focal_element_items = set(focal_element.split(","))
            for fe1, mass1 in bba1.items():
                items1 = set(fe1.split(","))
                for fe2, mass2 in bba2.items():
                    items2 = set(fe2.split(","))
                    intersection = items1 & items2
                    if len(intersection) == 0:
                        divisor += mass1 * mass2
                    elif focal_element_items == intersection:
                        quotient += mass1 * mass2
            if divisor == 1:
                # print("---------------")
                # print(focal_element)
                # print(bba1)
                # print(bba2)
                # print("---------------")
                result = 0
            else:
                result = quotient / (1 - divisor)
            bba[focal_element] = round(result, 3)
        return bba

    @staticmethod
    def pignistic(focal_element, bba):
        pr = 0
        for fe, mass in bba.items():
            pr += len(focal_element & set(fe.split(","))) / len(set(fe.split(","))) * mass
        return pr