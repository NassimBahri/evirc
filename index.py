from edb import transform, train_test_split_save, cross_validation
from evi_association_rules.Fimed import Fimed
from evi_rules_classifier.EvAC import EvAC, RuleFilter
from evi_rules_classifier.EviRC import EviRC
from evi_rules_classifier.DsARM import DsARM
import json
from metrics import accuracy
import os

dataset = "dataset/weather/weather-1"
class_index = 5
domain = ["1", "2"]

# Create training and testing datasets
with open(dataset+".json") as file:
    data = json.load(file)
train_test_data = cross_validation(data, n_splits=5, random_state=1)
print("## Step 2: Train and test data generated")
# Run the data set
accuracy_evac = 0
accuracy_evirc = 0
accuracy_ds = 0
for ds in train_test_data:
    with open(dataset+"_train_cross.json", 'w') as file:
        json.dump(ds["train_data"], file)
    # Generate rules
    fimed = Fimed(dataset + "_train_cross.json", min_support=0.1, min_confidence=0.5)
    print("## Step 3: Rules generated")
    x_test = ds["test_data"]
    y_test = ds["test_labels"]
    # Create the associative classifier (EvAC)
    evac_classifier = EvAC(fimed.rules, class_attribute=str(class_index), class_domain=domain,
                           rules_filter=RuleFilter.GENERIC)
    # Classify instances (EvAC)
    predictions = evac_classifier.predict(x_test)
    accuracy_evac += accuracy(y_test, predictions)
    # Create the associative classifier (EviRC)
    evi_classifier = EviRC(fimed.rules, class_attribute=str(class_index), class_domain=domain)
    # Classify instances (EviRC)
    predictions = evi_classifier.predict(x_test)
    accuracy_evirc += accuracy(y_test, predictions)
    # Create the associative classifier (DsARM)
    ds_classifier = DsARM(fimed.rules, class_attribute=str(class_index), class_domain=domain, distance_threshold=2)
    # Classify instances (DsARM)
    predictions = ds_classifier.predict(x_test)
    accuracy_ds += accuracy(y_test, predictions)
print(f"Precision of the EvAC algorithm: {accuracy_evac / len(train_test_data) * 100}% / unclassified instances: {evac_classifier.unclassified_instances}")
print(f"Precision of the EviRC algorithm: {accuracy_evirc / len(train_test_data) * 100}% / unclassified instances: {evi_classifier.unclassified_instances}")
print(f"Precision of the DsARM algorithm: {accuracy_ds / len(train_test_data) * 100}% / unclassified instances: {ds_classifier.unclassified_instances}")
