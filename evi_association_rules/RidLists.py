import json


class RidLists:
    """
    The RidLists class is a vertical representation of the evidential database
    Reference: Bach Tobji, MA, B. Ben Yaghlane, and Khaled Mellouli.
    "A new algorithm for mining frequent itemsets from evidential databases."
    Proceedings of IPMU. Vol. 8. 2008.

    Attributes
    ----------
    rid : dict
        Rid Lists
    size : int
        Size of the database (number of instances)

    Methods
    -------
    __init__(file_path)
        Constructor of the class
    generate(file_path)
        Create the RidLists structure
    update()
        Update the RidLists structure
    display(html_file_path)
        Display the Rid Lists in HTML file
    """

    def __init__(self, file_path):
        """Constructor of the class

        Parameters
        ----------
        file_path : string
            Path to the json data file
        """
        self.rid = {}
        self.size = 0
        self.generate(file_path)
        self.update()

    def generate(self, file_path):
        """Create the RidLists structure

        Parameters
        ----------
        file_path : str
            Path to the json data file
        """
        with open(file_path) as file:
            data = json.load(file)
            rowid = 1
            for line in data:
                for attribute, focal_elements in line.items():
                    for fe, mass in focal_elements.items():
                        if mass == 0:
                            continue
                        value = attribute+"#"+fe
                        if value not in self.rid:
                            self.rid[value] = []
                        self.rid[value].append(Item(rowid, mass, mass))
                rowid += 1
            self.size = rowid - 1

    def update(self):
        """Update the RidLists structure
        """
        keys = self.rid.keys()
        keys = sorted(keys, key=lambda element: len(element.split(",")), reverse=True)
        for key in keys:
            items = self.rid[key]
            values = key.split("#")
            ratings = values[1].split(",")
            if len(ratings) == 1:
                continue
            for key2, items2 in self.rid.items():
                if key2.startswith(values[0]+"#"):
                    values2 = key2.split("#")
                    ratings2 = values2[1].split(",")
                    if len(key) < len(key2) or key == key2:
                        continue
                    is_subset = all(elem in ratings for elem in ratings2)
                    if is_subset:
                        for item in items2:
                            exists = False
                            i = 0
                            length = len(items)
                            while i < length and not exists:
                                if item.rowid == items[i].rowid:
                                    exists = True
                                    items[i].bel += item.mass
                                i += 1
                            if not exists:
                                items.append(Item(item.rowid, item.mass, item.bel))

    def display(self, html_file_path):
        """Display the Rid Lists in HTML file

        Parameters
        ----------
        html_file_path : str
            Path to the store the output file
        """
        html = open(html_file_path, "w")
        content = """<html><head><style>table{width:100%;border:solid 1px;border-bottom:none;border-right:none} 
                        table td, table th{border-right:solid 1px #000;border-bottom:solid 1px #000;
                        text-align:center;vertical-align:middle;} </style></head><body><table cellpadding="0" cellspacing="0"><tr>
                        <th>Item</th><th>Rid list</th>"""
        for focal_element, items in self.rid.items():
            content += "<tr><th>"+focal_element+"</th><td>"
            for item in items:
                content += "("+str(item.rowid)+", "+str(item.bel)+") "
            content += "</td></tr>"
        content += "</table></body></html>"
        html.write(content)
        html.close()


class Item:
    """
    The Item class stores for each focal element its row identifier, its mass and
    its belief value.

    Methods
    -------
    __init__(rowid, mass, bel)
        Constructor of the class
    """

    def __init__(self, rowid, mass, bel):
        """Constructor of the class

        Parameters
        ----------
        rowid : int
            Row identifier of the focal element
        mass : float
            Mass of the focal element
        mass : float
            Belief of the focal element
        """
        self.rowid = rowid
        self.mass = mass
        self.bel = bel
